﻿using System;
namespace Ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] num1 = new int[10];
            int x, mayor = 0, c = 0;
            for (x = 0; x < 10; x++)
            {
                Console.Write("Inserte el valor de la Posicion Número {0}:", (x + 1));
                num1[x] = Convert.ToInt32(Console.ReadLine());

            }
            while (c < 10)
            {
                if (num1[c] > mayor)
                    mayor = num1[c];
                c++;

            }
            Console.WriteLine();
            Console.WriteLine("El Mayor es: {0} ", mayor);
            Console.ReadKey();
        }
    }
}
﻿using System;

namespace ejercicio9
{
    class TablaMultiplicar
    {
        public void CargarValor()
        {
            int valor;
            string linea;
            
            do
            {
                Console.Write("Insertar un Número. Para Salir Escribir -1: ");
                linea = Console.ReadLine();
                valor = int.Parse(linea);
                if (valor != -1)
                {
                    m1(valor);
                }
                else
                {
                    Console.WriteLine("Ha insertado -1 y a Concluido el Programa");
                }
            } while (valor != -1);
        }

        public void m1(int v)
        {
            int num1 = 1;
            int fx;
            for (int f = v; num1 <= 12;num1++)
            {
                fx = num1 * f;
                Console.WriteLine(f + " X " + num1 + " = " + fx);
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            TablaMultiplicar tm = new TablaMultiplicar();
            tm.CargarValor();
        }
    }
}
﻿using System;
namespace programa6
{
    class Program
    {
        static Image[] ig = new Image[700];
        static int igcontador = 0;
        static void Main(string[] args)
        {
            StartProgram();
        }

        static void StartProgram()
        {
            Console.ResetColor();
            int option = 0;

            while (option != 1 && option != 2 && option != 3)
            {
                Console.WriteLine("Ingresa");
                Console.WriteLine();
                Console.WriteLine("1: Añadir ficha");
                Console.WriteLine("2: Ver fichas");
                Console.WriteLine("3: Buscar ficha");
                Console.WriteLine();
                int.TryParse(Console.ReadLine(), out option);

                if (option == 1)
                {
                    if (igcontador > 698)
                    {
                        Console.WriteLine("Maximo Número de Imagenes Alcanzado");
                    }
                    else
                    {
                        AddFicha();
                    }
                }
                else if (option == 2)
                {
                    ListFichas();
                }
                else if (option == 3)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(SearchFicha().getProperties());
                }
            }

            StartProgram();
        }


        static void AddFicha()
        {
            int width, height;
            double size;
            string name;
            Console.ForegroundColor = ConsoleColor.Green;

            Console.Write("Ingrese el Nombre de la Imagen: ");
            name = Console.ReadLine();

            Console.Write("Ingrese el Ancho de la Imagen: ");
            int.TryParse(Console.ReadLine(), out width);

            Console.Write("Ingrese el Alto de la Imagen: ");
            int.TryParse(Console.ReadLine(), out height);

            Console.Write("Ingrese el Tamaño de la Imagen en kb: ");
            double.TryParse(Console.ReadLine(), out size);

            ig[igcontador] = new Image(name, width, height, size);
            igcontador++;
        }

        static void ListFichas()
        {
            for (int i = 0; i < igcontador; i++)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine((i + 1) + ". " + ig[i].getProperties());
            }
        }

        static Image SearchFicha()
        {
            Console.Write("Ingresa el Nombre de la Ficha ");
            string name = Console.ReadLine();
            foreach (Image image in ig)
            {
                if (image.Name == name)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    return image;
                }
            }

            return new Image(null, 0, 0, 0);
        }
    }

    class Image
    {
        private int _width;
        private int _height;
        private double _size;

        public Image(string name, int width, int height, double size)
        {
            this.Name = name;
            this._width = width;
            this._height = height;
            this._size = size;
        }

        public string getProperties()
        {
            return string.Format($"nombre: {this.Name} \b ancho: {this._width}px \b alto: {this._height}px \b tamaño: {this._size}Kb");
        }

        public string Name { get; private set; }
    }
}
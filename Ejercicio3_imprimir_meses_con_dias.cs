﻿using System;

namespace ConsoleApp58
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1;
            string[] m1 = new string[12];
            m1[0] = "Enero Tiene 31 Dias";
            m1[1] = "Febrero Tiene 28 Dias";
            m1[2] = "Marzo Tiene 31 Dias";
            m1[3] = "Abril Tiene 30 Dias";
            m1[4] = "Mayo Tiene 31 Dias";
            m1[5] = "Junio Tiene 30 Dias";
            m1[6] = "Julio Tiene 30 Dias";
            m1[7] = "Agosto Tiene 31 Dias";
            m1[8] = "Septiembre Tiene 30 Dias";
            m1[9] = "Octubre Tiene 31 Dias";
            m1[10] = "Noviembre Tiene 30 Dias";
            m1[11] = "Diciembre Tiene 31 Dias";


            

            Console.WriteLine("Seleccione un Mes: ");
            Console.WriteLine();
            Console.WriteLine("0=Enero");
            Console.WriteLine("1=Febrero");
            Console.WriteLine("2=Marzo");
            Console.WriteLine("3=Abril");
            Console.WriteLine("4=Mayo");
            Console.WriteLine("5=Junio");
            Console.WriteLine("6=Julio");
            Console.WriteLine("7=Agosto");
            Console.WriteLine("8=Septiembre");
            Console.WriteLine("9=Octubre");
            Console.WriteLine("10=Noviembre");
            Console.WriteLine("11=Diciembre");
            Console.WriteLine();
            num1 = int.Parse(Console.ReadLine());

            Console.WriteLine(m1[num1]);
        }
    }
}
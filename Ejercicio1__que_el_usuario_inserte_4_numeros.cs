﻿using System;

namespace ConsoleApp56
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Insertar, 4 Valores: ");
            Console.WriteLine();

            int[] num1 = new int[4];
            
            for (int x = 0; x < 4; x++)
            {
                num1[x] = int.Parse(Console.ReadLine());
            }

            int num2;
            num2 = num1[0] + num1[1] + num1[2] + num1[3];
            int num3 = num2/2 + num2;

            Console.WriteLine("La Media Aritmetica de los Datos Introducidos es: {0} ", num3);
            Console.WriteLine();

            num2 = num2 / 2;
            Console.WriteLine("Su Media es :{0}", num2);
            Console.WriteLine();

            for (int x = 0; x < 4; x++)
            {
                Console.WriteLine(num1[x]);
            }
            
        }
    }
}

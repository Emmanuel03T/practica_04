﻿using System;
namespace Ejercicio7
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            Console.ForegroundColor = ConsoleColor.Green;

            Console.Write("Inserte un Nombre: ");
            name = Console.ReadLine();

            int b = name.Length;

            for (int a = 0; a < b; a++)
            {
                Console.Write("{1} ", a, name[a]);
            }
        }
    }
}
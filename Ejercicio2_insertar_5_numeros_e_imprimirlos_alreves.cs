﻿using System;

namespace ConsoleApp50
{
    class Program
    {
        static void Main(string[] args)
        {
            float[] n2 = new float[5];

            Console.WriteLine("Itroduzca 5 Números");

            Console.WriteLine("\n\nInserte el Primer número: ");
            n2[0] = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine("\n\nInserte el Segundo número: ");
            n2[1] = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine("\n\nInserte el Tercero número: ");
            n2[2] = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine("\n\nInserte el Cuarto número: ");
            n2[3] = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine("\n\nInserte el Quinto número: ");
            n2[4] = Convert.ToSingle(Console.ReadLine());

            Console.WriteLine("\n\nLos números Insertados alrevés son:  {4}, {3}, {2}, {1}, {0}", n2[0], n2[1], n2[2], n2[3], n2[4]);
            Console.ReadLine();
        }
    }
}

﻿using System;
namespace Ejercicio10
{
    class Persona
    {
        private string nombre;
        private int edad;

        public void Inicializar()
        {
            Console.Write("Introduzca un el Nombre:");
            nombre = Console.ReadLine();
            string linea;
            Console.Write("Entre su Edad:");
            linea = Console.ReadLine();
            edad = int.Parse(linea);
        }

        public void Imprimir()
        {
            Console.Write("Nombre:");
            Console.WriteLine(nombre);
            Console.Write("Edad:");
            Console.WriteLine(edad);
        }

        public void EsMayorEdad()
        {
            if (edad >= 18)
            {
                Console.Write("Es mayor de Edad");
            }
            else
            {
                Console.Write("No es mayor de Edad");
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            Persona p1 = new Persona();
            p1.Inicializar();
            p1.Imprimir();
            p1.EsMayorEdad();
        }
    }


}